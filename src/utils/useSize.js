import { useState, useEffect } from "react"
const isBrowser = typeof window !== "undefined"

export default function useSize() {
  const [isMobile, setMobile] = useState(isBrowser?(window.innerWidth < 576):false);

  useEffect(() => {
    function handleResize() {
      setMobile(window.innerWidth < 576)
    }
    window.addEventListener("resize", handleResize);
    // handleResize();
    return () => window.removeEventListener("resize", handleResize);
  }, []); 

  return isMobile;
}