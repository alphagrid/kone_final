import { useCallback, useEffect, useRef, useState } from 'react';

function useScale() {
  const isMounted = useRef(true);
  const isSsr = typeof window === 'undefined';
  const [scale, setScale] = useState(isSsr ? 1 : getScale() );




  function getScale(){
    let scale = window.innerHeight / 980
    if(scale > 1) scale = 1

    return scale
  } 


  const handleResize = useCallback(() => {
    if (isMounted.current) {
      setScale(getScale());
    }
  }, [setScale]);

  useEffect(() => {
    window.addEventListener('resize', () => {
      window.requestAnimationFrame(handleResize);
    });
    return () => {
      isMounted.current = false;
      window.removeEventListener('resize', handleResize);
    };
  }, [handleResize]);

  return scale;
}

export default useScale;