module.exports = {
  // pathPrefix: ``,
  // assetPrefix: `https://kone.alphagridinfographics.com`,
  siteMetadata: {
    title: `Kone`,
    description: `Next-level Cities`,
    author: `@mahimka`,
  },
  flags: { PRESERVE_WEBPACK_CACHE: true },
  plugins: [
    `gatsby-plugin-webpack-bundle-analyser-v2`,
    `gatsby-plugin-remove-fingerprints`,
    `gatsby-plugin-smoothscroll`,
    `gatsby-plugin-react-svg`,
    `gatsby-plugin-postcss`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-gatsby-cloud`,
    `@wardpeet/gatsby-plugin-static-site`,
  ],
}